Accessing the code
=====================

The SPEXone processor is currently hosted at Bitbucket and can be cloned by issuing

.. code-block:: bash

   git clone git@bitbucket.org:sron_earth/spexone_cal.git

If you encounter an error with this command, make sure the SSH key of your computer has been uploaded to Bitbucket. For that, log in at https://bitbucket.org, go to `Personal settings -> SSH keys -> Add key`, pick any label, and copy the the contents of your public key, typically found at ``~/.ssh/id_rsa.pub``. This needs to be done separately for every machine from which you want to access the repository.

The source code of this documentation is hosted at https://bitbucket.org/sron_earth/spexone_cal_doc/.

Also, make sure to join the SPEXone Slack channel! This is the most efficient way to communicate with people involved with this project. Ask any of the developers to provide you access to the Slack channel.
