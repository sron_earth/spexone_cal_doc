Internals
==========

Notes on parallelization
------------------------

The L1A-L1B step of the processor has been parallelized using MPI. Parallelization is performed over images.

Regression tests
----------------

Regression tests are run in a Docker container which currently runs in a developer's computer. The container has been generated in a two-step process:

- The first container, `raullaasner/spexone-environment`, contains the main environment for running the L1A-L1C processor. Its build file is located at `CI/docker_image/Dockerfile`. See the comments at the beginning of that file for more details.
- The second container, `raullaasner/spexone_cal-testing`, builds on the first one but adds test data for running the tests. The build file for this container is not currently publicly available.

The reason the containers are split into one that provides the build environment and a second one that provides the test data is to make the first one also usable in other projects. For instance, the SPEXone instrument model has different regression tests but the same build environment.

The container that contains the build environment, `raullaasner/spexone-environment`, makes use of the Spack package manager. Spack is designed for managing scientific packages and is highly flexible when installing packages with very specific build configurations. Packages that are installed by Spack in the container are listed in `CI/docker_image/spack.yaml`. Whenever there is an update in the SPEXone dependency list it suffices to update that file and rebuild the container. Other packages that the SPEXone processor depends on, not maintained by Spack, are built in later steps in the container (see GADfit in `CI/docker_image/Dockerfile`) and any configuration files they might require are in the same directory.
