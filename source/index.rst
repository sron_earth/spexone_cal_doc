===========================
SPEXone data processor
===========================

SPEXone data processor is a calibration program for the SPEXone instrument. It can derive the calibration key data (CKD) and process flight or calibration data levels L1A to L1C. This documentation contains instructions on how to build and run the software. Details about different processor steps and the theory behind them are provided elsewhere ("SPEXone L1A-L1C processor: Algorithm theoretical baseline document", currently not publicly available).

Contents
---------

.. toctree::
   :maxdepth: 2

   accessing_code
   build_process
   running_the_code
   contributing
   internals

Terms of use
-------------

The SPEXone processor is licensed under the 3-clause BSD license found in the LICENSE file in the root directory of the project.

Contributors
-------------

In order to see a list of contributors, clone the repository and issue

.. code-block:: bash

   git shortlog -s -n --all --no-merges

This prints a list of contributors along with a number of commits.
