Running the code
====================

If the build process was successful, an executable called `spexone` is produced in the build directory `<build>`. The executable takes one argument which a configuration file that specifies steps for either *i)* generating a CKD file or *ii)* applying the CKD to images in flight mode:

.. code-block:: bash

   <build>/spexone run.yaml

where `run.yaml` is a user defined configuration file. If the processor was compiled using a parallel C++ compiler, it can be run by issuing

.. code-block:: bash

   mpirun -np <n> <build>/spexone run.yaml

where `<n>` is the number of MPI tasks.

YAML configuration files
-------------------------

SPEXone configuration files are written in YAML. We chose YAML because it is versatile, human-readable, and widely used and supported. It uses Python-style indentation to indicate nesting and as basic types supports scalars, lists, and maps (also called dictionaries or hashes). Furthermore, it is a strict superset of JSON and thus compatible with it.

In order to understand the YAML format open one of the tutorial configuration files or run the processor without an argument. You may direct it into a file like this:

.. code-block:: bash

   <build>/spexone > conf.yaml

The resulting `conf.yaml` contains keywords with their default values that are recognized by the processor. Many default values are not suitable and must be edited before feeding the configuration file to the processor. The configuration file contains several sections: a `main` section for parameters that are revelant for all processes and a section for each process (a CKD generation step, L1B, or L1C process) seprately. There is also a section called `verbose` which explains in detail what every parameter means and how to use it. The verbose section is not recognized by the processor and is only generated to provide information to the user.

The basic element of YAML is a key-value pair where the value is a scalar, list, or a map. Here is an example where the element type is noted as a comment::

  main: # starts a map
    processes: # key followed by a list
      - pol # scalar
      - l1b # scalar
  ckd_file_in: ckd.nc # key-value
  detector_spec: 2048 # key-value

When you're done editing the configuration file, run the processor with the argument:

.. code-block:: bash

   <build>/spexone conf.yaml

There are many checks in the code which notify you of errors and inconsistencies in the configuration file. If you use a non-existent keyword the processor will issue a warning but will continue with the calculation.

Full specification of YAML and how to use it is found here: https://yaml.org/spec/1.2.2/.

Example usage of the processor on SRON computers
-------------------------------------------------------

Example configuration files for running different parts of the processor are located at

.. code-block:: bash

   /nfs/SPEXone/users/raull/spexone

Start by reading `Readme.md` found in that directory and then proceed to run one of the simulations. For instance, in order to regenerate the CKD create a new working directory and copy the configuration files into that directory:

.. code-block:: bash

   cp /nfs/SPEXone/users/raull/spexone/1_CKD_one_run/*yaml .

Then run the calculation:

.. code-block:: bash

   <build>/spexone ckd.yaml

which will create a file called `ckd.nc` in the current directory. It should have the same contents as the reference CKD file from the `1_CKD_one_run` directory. Also in that directory you will find the command that was used to generate the reference file (see `Readme.md`) and the corresponding log file.
